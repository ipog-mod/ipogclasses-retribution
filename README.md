[Symbol_Game]: https://gitlab.com/ipog-mod/ipogclasses/-/raw/master/Sprites/Other/IJWLC0
[Symbol_Important]: https://gitlab.com/ipog-mod/ipogclasses/-/raw/master/Graphics/IPOGFORW


# IPOG Classes - Retribution Mode

### ![Symbol_Important] IMPORTANT: This requires [IPOG Classes](https://gitlab.com/ipog-mod/ipogclasses) to work!

## ![Symbol_Game]**Description**

It's a game mode developed for the "IPOG Classes" mod. It simulates an invasion game mode by replacing deathmatch starts with Red Enemy Portals.


## ![Symbol_Game]Custom CVar(s)

#### ![Symbol_Important] NOTE: All CVars are dynamic, meaning they all take effect after changed or as soon as possible.

**IPOG_Retribution <0|1\>:** Activates a new game mode: Retribution. Red Enemy Portals are activated on DM starts, and the only way out is via Exit Nexus. It's only compatible with maps with DM starts. Server hosts should also enable these cvars to work as intended:

**sv_killallmonsters 1**

**sv_nomonsters 1**


## ![Symbol_Game]Licensing

In Pursuit of Greed Retribution project is licensed under the [GPL-3](http://www.gnu.org/licenses/gpl.html).

The data files (artwork, sound files, etc) are not covered in this license. The ones that came from In Pursuit of Greed have a [custom license](https://archive.org/details/GreedSource):

"These sources are provided for educational and historical purposes. No assets or code may be used in any way commercially. Personal and educational use only."
